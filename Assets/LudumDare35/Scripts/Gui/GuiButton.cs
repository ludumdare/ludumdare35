﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Ludum35.Gui
{
    public class GuiButton : MonoBehaviour
    {
        public Sprite[] ButtonSprites;
        public int FramesRate = 5;

        private Coroutine _runner;

        void Start()
        {
            _runner = StartCoroutine(runner());
        }

        private IEnumerator runner()
        {
            int frame = 0;
            while(true)
            {
                GetComponent<Image>().sprite = ButtonSprites[frame];
                yield return new WaitForSeconds(FramesRate * Time.deltaTime);

                frame += 1;
                if (frame > ButtonSprites.Length-1)
                {
                    frame = 0;
                }
            }
        }

    }
}
