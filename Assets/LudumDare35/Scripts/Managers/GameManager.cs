﻿using UnityEngine;
using System.Collections;
using Ludum35.Controllers;
using Ludum35.Engines;
using Ludum35.Data;

namespace Ludum35.Managers
{
    public class GameManager : MonoSingleton<GameManager>
    {
        public SoundController SoundController { get; set; }
        public InputController InputController { get; set; }
        public MusicController MusicController { get; set; }
        public GameController GameController { get; set; }
        public ShifterController ShifterController { get; set; }
        public CursorController CursorController { get; set; }
        public GuiController GuiController { get; set; }

        public GameEngine GameEngine { get; set; }
        public LevelEngine LevelEngine { get; set; }

        public GameData GameData { get; set; }

        public void LoadGameData()
        {
            GameData = ScriptableObject.CreateInstance<GameData>();

            GameData.UnlockedThemes = 1;
            GameData.HiScore = 0;
            GameData.Shapes = 0;

            //SaveGameData();

            if (PlayerPrefs.HasKey("gameData"))
            {
                string s = PlayerPrefs.GetString("gameData");
                JsonUtility.FromJsonOverwrite(s, GameData);
            }
        }

        public void SaveGameData()
        {
            PlayerPrefs.SetString("gameData", JsonUtility.ToJson(GameData).ToString());
            PlayerPrefs.Save();
        }

        void OnEnable()
        {
            LoadGameData();
        }

        public int Score { get; set; }
        public int Lives { get; set; }
        public int Level { get; set; }
        public int Shapes { get; set; }
    }
}
