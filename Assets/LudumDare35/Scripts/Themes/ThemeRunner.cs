﻿using UnityEngine;
using System.Collections;
using Ludum35.Data;

namespace Ludum35.Themes
{
    public class ThemeRunner : MonoBehaviour
    {
        public SpriteRenderer BackgroundSpriteRenderer;
        public ThemeData ThemeData;

        public virtual void RegisterRunner()
        {
            D.Trace("[ThemeRunner] RegisterRunner");
        }

        public virtual void StartRunner()
        {
            D.Trace("[ThemeRunner] StartRunner");
            BackgroundSpriteRenderer.sprite = ThemeData.ThemeImage;
        }

        public virtual void StopRunner()
        {

        }

    }
}
