﻿using UnityEngine;
using System.Collections.Generic;
using Ludum35.Managers;
using Ludum35.Data;
using Ludum35.Controllers;

namespace Ludum35.Engines
{
    public class LevelEngine : MonoBehaviour
    {
        public LaneController Lane;
        public GameObject LaneRoot;
        public LevelDatabase LevelDatabase;

        private ThemeData _theme;
        private SwatchData _swatch;
        private LevelData _levelData;

        public LevelData CreateLevel(int level, ThemeData theme, SwatchData swatch)
        {
            D.Trace("[LevelEngine] GetLevel");

            //  set the theme and color swatch from the theme
            _theme = theme;
            _swatch = swatch;

            //  get the level
            _levelData = LevelDatabase.Levels[level-1];

            if (_levelData == null)
                D.Error("- no level data was found");

            return _levelData;
        }

        public List<LaneController> CreateLanes(int lanes)
        {
            //  create the lanes
            return createLanes(lanes);
        }

        private List<LaneController> createLanes(int lanes)
        {
            D.Trace("[LevelEngine] createLanes");

            int startPosX = 150 - (24 * (lanes-1));
            int startPosY = 0;

            List<LaneController> list = new List<LaneController>();
            for (int i = 0; i < lanes; i++)
            {
                LaneController lc = Instantiate(Lane);
                lc.transform.parent = LaneRoot.transform;
                lc.transform.localPosition = new Vector2(startPosX, startPosY);
                list.Add(lc);
                startPosX += 48;
                lc.Theme = _theme;
                lc.Swatch = _swatch;
                lc.SetShape(_levelData.Shapes, _levelData.Colors);
            }

            return list;
        }

        void OnEnable()
        {
            D.Trace("[LevelEngine] OnEnable");
            GameManager.Instance.LevelEngine = this;
        }

        void OnDisable()
        {
        }
    }
}