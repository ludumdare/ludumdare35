﻿using DG.Tweening;
using Ludum35.Controllers;
using Ludum35.Data;
using Ludum35.Managers;
using Ludum35.Themes;
using Sdn.SpriteBoss;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;

namespace Ludum35.Engines
{
    public class GameEngine : MonoBehaviour
    {
        public ThemeDatabase ThemeDatabase;
        public SwatchData SwatchData;
        public SpriteRenderer BackgroundSpriteRenderer;
        public ShapeController ShapePrefab;

        private bool _active;
        private bool _paused;
        private List<LaneController> _laneControllers;
        private ThemeData _themeData;
        private ThemeRunner _themeRunner;
        private List<IPauseable> _pauseableItems;

        private Coroutine _laneRunner;
        private LevelData _levelData;

        public List<LaneController> LaneControllers
        {
            get { return _laneControllers; }
        }

        public void StartGame()
        {
            D.Trace("[GameEngine] StartGame");

            GameManager.Instance.Lives = 3;
            GameManager.Instance.Score = 0;
            GameManager.Instance.Shapes = 0;
            GameManager.Instance.Level = 1;

            GameManager.Instance.GuiController.UpdateGui();
            GameManager.Instance.CursorController.Show();

            StartLevel(GameManager.Instance.Level);
        }

        //  when the player starts the game
        public void StartLevel(int level)
        {
            D.Trace("[GameEngine] StartLevel");

            GameManager.Instance.GuiController.UpdateGui();

            //  get the theme
            _themeData = ThemeDatabase.Get(Random.Range(0, GameManager.Instance.GameData.UnlockedThemes));

            //  in case i missed one :)
            if (_themeData == null)
                _themeData = ThemeDatabase.Get("Sample");

            //  get the next level data
            _levelData = GameManager.Instance.LevelEngine.CreateLevel(level, _themeData, SwatchData);
            GameManager.Instance.GameController.LevelData = _levelData;

            StartCoroutine(levelIntro());
        }

        public IEnumerator levelIntro()
        {
            D.Trace("[GameEngine] StartLevel");
            GameManager.Instance.GuiController.ShowNextLevel();

            //  get the shape lanes for the next level
            _laneControllers = GameManager.Instance.LevelEngine.CreateLanes(_levelData.Lanes);


            //  set the theme object and start it
            _themeRunner = Instantiate(_themeData.ThemeRunner);
            _themeRunner.BackgroundSpriteRenderer = BackgroundSpriteRenderer;
            _themeRunner.ThemeData = _themeData;
            _themeRunner.StartRunner();

            //  start the music
            GameManager.Instance.MusicController.PlayMusic("Game", 0.05f);

            yield return new WaitForSeconds(3);
            GameManager.Instance.GuiController.HideAll();

            //  start the lane runner
            _laneRunner = StartCoroutine(laneRunner(_levelData.Total, _levelData.Speed));

            //  start the input controller
            GameManager.Instance.InputController.StartController();
            GameManager.Instance.CursorController.Show();

            GameManager.Instance.GameController.Init();

            yield return null;
        }

        //  when the player wants to pause
        public void PauseGame()
        {
            D.Trace("[GameEngine] PauseGame");
        }

        //  when the player wants to resume
        public void ResumeGame()
        {
            D.Trace("[GameEngine] ResumeGame");
        }

        //  when the player has ended the game
        public void EndGame()
        {
            D.Trace("[GameEngine] EndGame");
            GameManager.Instance.SoundController.PlaySound("Over");
            GameManager.Instance.GuiController.ShowGameOver();
            GameManager.Instance.InputController.StopController();
            GameManager.Instance.CursorController.Hide();
            _active = false;
            StopCoroutine(_laneRunner);
            _themeRunner.StopRunner();
            destroyShapes();
            DestroyLanes();
            _laneControllers = null;
        }

        public void EndLevel()
        {
            D.Trace("[GameEngine] EndLevel");
            GameManager.Instance.SoundController.PlaySound("Success");
            GameManager.Instance.ShifterController.Idle();
            GameManager.Instance.GuiController.ShowEndLevel();
            GameManager.Instance.InputController.StopController();
            GameManager.Instance.CursorController.Hide();
            _active = false;

            //if (_laneRunner != null)
                StopCoroutine(_laneRunner);

            _themeRunner.StopRunner();
            destroyShapes();
            DestroyLanes();
            _laneControllers = null;
            GameManager.Instance.Level += 1;
            GameManager.Instance.SaveGameData();
            StartLevel(GameManager.Instance.Level);
        }

        public void ExitGame()
        {
             SceneManager.LoadScene("Home");
        }

        private void DestroyLanes()
        {
            if (_laneControllers == null)
                return;

            foreach (LaneController lc in _laneControllers)
            {
                lc.DestroyLane();
            }
        }

        private void destroyShapes()
        {
            foreach(GameObject g in GameObject.FindGameObjectsWithTag("Shape"))
            {
                Destroy(g.gameObject);
            }
        }

        //  run the lanes
        private IEnumerator laneRunner(int shapesLimit, float speed)
        {
            D.Trace("[GameEngine] laneRunner");
            int shapes = 0;
            _active = true;

            while (_active && shapes < shapesLimit)
            {
                if (!_paused)
                {
                    D.Log("- another shape is falling");
                    shapes += 1;
                    createShape();
                }
                yield return new WaitForSeconds(speed);
            }
            endLevel();
            yield return null;
        }

        private void createShape()
        {
            //  get a random lane
            int r = Random.Range(0, _laneControllers.Count);
            LaneController lc = _laneControllers[r];

            ShapeController sc = Instantiate(ShapePrefab);
            sc.transform.localScale = Vector3.zero;
            sc.transform.position = lc.LaneStart.transform.position;
            sc.LaneId = lc.Id;
            SetShape(sc, _levelData.Shapes, _levelData.Colors);
            StartCoroutine(shapeRunner(sc));
        }

        private IEnumerator shapeRunner(ShapeController shapeController)
        {
            if (shapeController != null)
                yield return shapeController.transform.DOScale(Vector3.one, _levelData.DropSpeed).WaitForCompletion();

            if (shapeController != null)
                yield return shapeController.transform.DOShakePosition(1.0f, 5.0f);

            if (shapeController != null)
                shapeController.GetComponent<SpriteBoss>().MoveAlongVector(Vector2.down, _levelData.DropSpeed);
        }

        public void SetShape(ShapeController shapeController, int shapes, int colors)
        {
            if (shapes == -1)
                shapes = _themeData.ThemeSprites.Count;

            if (colors == -1)
                colors = SwatchData.SwatchColor.Count;

            int spriteIndex = Random.Range(0, shapes);
            int colorIndex = Random.Range(0, colors);
            shapeController.GetComponent<SpriteRenderer>().sprite = _themeData.ThemeSprites.Get(spriteIndex).Sprites[0];
            shapeController.GetComponent<SpriteRenderer>().color = SwatchData.SwatchColor[colorIndex];
            shapeController.ColorIndex = colorIndex;
            shapeController.ShapeIndex = spriteIndex;
        }

        private void endLevel()
        {
            D.Trace("[GameEngine] endLevel");
            EndLevel();
        }

        private void OnEnable()
        {
            D.Trace("[GameEngine] OnEnable");
            GameManager.Instance.GameEngine = this;
        }

        private void OnDisable()
        {
        }
    }
}