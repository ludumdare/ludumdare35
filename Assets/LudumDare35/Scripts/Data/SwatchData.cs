﻿using UnityEngine;
using System.Collections.Generic;

namespace Ludum35.Data
{
    [CreateAssetMenu]
    [System.Serializable]
    public class SwatchData : ScriptableObject
    {
        [SerializeField]
        public List<Color> SwatchColor;
    }
}