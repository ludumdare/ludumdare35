﻿using UnityEngine;
using System.Collections;
using Sdn.Core.Database;

namespace Ludum35.Data
{
    [CreateAssetMenu]
    [System.Serializable]
    public class ThemeDatabase : ScriptableDatabase<ThemeData>
    {
        public ThemeData Get(string name)
        {
            for (int i = 0; i < Data.Count; i++)
            {
                if (name == Data[i].ThemeName)
                    return Data[i];
            }

            return null;
        }
    }
}
