﻿using UnityEngine;
using System.Collections.Generic;

namespace Ludum35.Data
{
    [CreateAssetMenu]
    [System.Serializable]
    public class LevelDatabase : ScriptableObject
    {
        [SerializeField]
        public List<LevelData> Levels;
    }

}