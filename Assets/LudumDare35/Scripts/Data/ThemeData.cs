﻿using UnityEngine;
using Sdn.SpriteBoss;
using Ludum35.Themes;

namespace Ludum35.Data
{
    [System.Serializable]
    public class ThemeData
    {
        [SerializeField]
        public string ThemeName;

        [SerializeField]
        public Color ThemeColor;

        [SerializeField]
        public Sprite ThemeImage;

        [SerializeField]
        public ThemeRunner ThemeRunner;

        [SerializeField]
        public SpritePackageData ThemeSprites;
    }
}