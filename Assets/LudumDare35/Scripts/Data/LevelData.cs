﻿using UnityEngine;
using System.Collections;

namespace Ludum35.Data
{
    [CreateAssetMenu]
    [System.Serializable]
    public class LevelData : ScriptableObject
    {
        //  total lanes to show
        [SerializeField]
        public int Lanes;

        //  total shapes to use in the theme
        [SerializeField]
        public int Shapes;

        //  total colors to use in the theme
        [SerializeField]
        public int Colors;

        //  speed of shapes coming - aka delay
        [SerializeField]
        public int Speed;

        [SerializeField]
        public int DropSpeed;

        //  total number of shapes for the level
        [SerializeField]
        public int Total;


    }
}