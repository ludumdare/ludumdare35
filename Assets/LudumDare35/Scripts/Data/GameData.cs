﻿using UnityEngine;
using System.Collections;

namespace Ludum35.Data
{
    
    [System.Serializable]
    public class GameData : ScriptableObject
    {
        [SerializeField]
        public int HiScore;

        [SerializeField]
        public int Shapes;

        [SerializeField]
        public int UnlockedThemes;

    }
}
