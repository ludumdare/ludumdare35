﻿using UnityEngine;
using System.Collections;
using Ludum35.Managers;
using DG.Tweening;
using Sdn.SpriteBoss;

namespace Ludum35.Controllers
{
    public class ShifterController : MonoBehaviour
    {
        public SpriteRenderer Head;
        public SpriteRenderer Torso;
        public SpriteRenderer Neck;
        public SpriteRenderer ArmL1;
        public SpriteRenderer ArmR1;
        public SpriteRenderer Legs;
        public SpriteRenderer EyeL1;
        public SpriteRenderer EyeR1;
        public GameObject JetpackExhaust;

        public Sprite[] Eyes;
        public Sprite[] Torsos;

        public Sprite Exhaust;

        private Coroutine _wave;
        private Coroutine _moveTo;
        private Coroutine _shakeHead;
        private Coroutine _jetpack;
        private Coroutine _hover;
        private Coroutine _paint;
        private Coroutine _shift;
        private Coroutine _idle;

        private int _faceFront = 1;

        private SpriteBoss _spriteBoss;

        public LaneController CurrentLane;

        public void Wave()
        {
            D.Trace("[ShiftController] Wave");
            if (_wave != null)
                StopCoroutine(_wave);

            _wave = StartCoroutine(wave());
        }

        public IEnumerator WaitWave()
        {
            D.Trace("[ShiftController] Wave");
            yield return StartCoroutine(wave());
        }

        private IEnumerator wave()
        {
            D.Trace("[ShiftController] wave");
            yield return new WaitForEndOfFrame();
            yield return rotateArm(ArmL1.gameObject, 235);
            //yield return ArmL1.transform.DORotate(new Vector3(0, 0, 235), 0.1f).WaitForCompletion();
            yield return ArmL1.transform.DORotate(new Vector3(0, 0, 290), 0.5f).SetLoops(5, LoopType.Yoyo).WaitForCompletion();
            yield return null;
            Idle();
        }

        public void Idle()
        {
            faceFront();
            Hover();
            _idle = StartCoroutine(idle());
        }

        public IEnumerator WaitIdle()
        {
            faceFront();
            Hover();
            yield return StartCoroutine(idle());
        }

        private IEnumerator idle()
        {
            yield return new WaitForEndOfFrame();

            while(true)
            {
                yield return rotateArm(ArmL1.gameObject, 345);
                yield return rotateArm(ArmR1.gameObject, 15);
                yield return new WaitForSeconds(Random.Range(5, 15));
                yield return wave();
                yield return new WaitForSeconds(Random.Range(5, 15));
                yield return move(new Vector2(Random.Range(120, 400), Random.Range(40, 120)));
            }
        }

        public void MoveTo(GameObject gameObject)
        {
            StartJetpack();            
            _moveTo = StartCoroutine(move(gameObject));
        }

        public IEnumerator WaitMoveto(GameObject gameObject)
        {
            StartJetpack();
            yield return StartCoroutine(move(gameObject));
        }

        public IEnumerator WaitMoveto(Vector2 position)
        {
            StartJetpack();
            yield return StartCoroutine(move(position));
        }

        private IEnumerator move(GameObject gameObject)
        {
            yield return transform.DOMove(gameObject.transform.position, 0.75f).WaitForCompletion();
            //yield return StartCoroutine(_spriteBoss.WaitMoveTo(gameObject.transform.position));
            StopJetpack();
        }

        private IEnumerator move(Vector2 position)
        {
            yield return transform.DOMove(position, 0.75f).WaitForCompletion();
            //yield return StartCoroutine(_spriteBoss.WaitMoveTo(position));
            StopJetpack();
        }

        public void ShakeHead()
        {
            faceFront();
            Hover();
            _shakeHead = StartCoroutine(shakeHead());
        }

        private IEnumerator shakeHead()
        {
            yield return rotateArm(Head.gameObject, -5, 0.5f);
            yield return Head.transform.DORotate(new Vector3(0, 0, 5), 0.5f).SetLoops(5, LoopType.Yoyo).WaitForCompletion();
            yield return rotateArm(Head.gameObject, 0, 0.15f);
            Idle();
        }


        public void StartJetpack()
        {
            StopAllCoroutines();
            _hover = StartCoroutine(exhaust(0.01f, 25));
        }

        public void StopJetpack()
        {
            Hover();
        }

        public void Hover()
        {
            StopAllCoroutines();
            _hover = StartCoroutine(hover(0.5f, 2));
            _hover = StartCoroutine(exhaust(0.1f, 25));
        }

        public void Paint()
        {
            faceBack();
            Hover();
            _paint = StartCoroutine(paint());
        }

        public IEnumerator WaitPaint()
        {
            faceBack();
            Hover();
            yield return StartCoroutine(paint());
        }

        private IEnumerator paint()
        {
            yield return rotateArm(ArmL1.gameObject, 235);
            yield return rotateArm(ArmR1.gameObject, 125);
            yield return ArmL1.transform.DORotate(new Vector3(0, 0, 290), 0.05f).SetLoops(3, LoopType.Yoyo);
            yield return ArmR1.transform.DORotate(new Vector3(0, 0, 70), 0.05f).SetLoops(3, LoopType.Yoyo).WaitForCompletion();
            Idle();
        }

        public void Shift()
        {
            faceBack();
            Hover();
            _shift = StartCoroutine(shift());
        }

        public IEnumerator WaitShift()
        {
            faceBack();
            Hover();
            yield return StartCoroutine(shift());
        }

        private IEnumerator shift()
        {
            yield return rotateArm(ArmL1.gameObject, 235);
            yield return rotateArm(ArmR1.gameObject, 125);
            yield return ArmL1.transform.DORotate(new Vector3(0, 0, 290), 0.05f).SetLoops(3, LoopType.Yoyo);
            yield return ArmR1.transform.DORotate(new Vector3(0, 0, 70), 0.05f).SetLoops(3, LoopType.Yoyo).WaitForCompletion();
            Idle();
        }

        private IEnumerator rotateArm(GameObject arm, int rotation, float duration = 0.5f)
        {
            yield return arm.transform.DORotate(new Vector3(0, 0, rotation), duration).WaitForCompletion();

        }

        //  delay = time between exhaust; shake = amount of shake during exhaust
        private IEnumerator hover(float delay, float shake)
        {
            Vector2 pos = transform.position;

            while (true)
            {
                float y = Random.Range(-shake, shake+1);
                float x = Random.Range(-shake, shake+1);
                yield return transform.DOMove(pos + new Vector2(x, y), delay).WaitForCompletion();    
            }
        }

        private IEnumerator exhaust(float delay, int distance)
        {
            while(true)
            {
                StartCoroutine(exhaustSmoke(distance));
                yield return new WaitForSeconds(delay);
            }
        }

        private IEnumerator exhaustSmoke(int distance)
        {
            GameObject go = new GameObject();
            go.transform.position = JetpackExhaust.transform.position;
            SpriteRenderer sr = go.AddComponent<SpriteRenderer>();
            sr.sortingLayerName = "Shifter";
            sr.sortingOrder = -50 * _faceFront;
            sr.sprite = Exhaust;
            float ry = Random.Range(distance, distance * 1.50f);
            float rx = Random.Range(-distance, distance+1) * 0.25f;
            go.transform.DOMoveY(transform.position.y - ry, 0.25f);
            go.transform.DOMoveX(transform.position.x - rx, 0.25f);
            go.transform.DOScale(0.0f, 0.25f);
            yield return new WaitForSeconds(2.0f);
            Destroy(go);
        }

        private void faceFront()
        {
            _faceFront = 1;
            EyeL1.sortingOrder = 99;
            EyeR1.sortingOrder = 99;
            Torso.sprite = Torsos[0];
            Head.flipX = false;
        }

        private  void faceBack()
        {
            _faceFront = -1;
            EyeL1.sortingOrder = -99;
            EyeR1.sortingOrder = -99;
            Torso.sprite = Torsos[1];
            Head.flipX = true;
        }


        void Start()
        {
            Idle();
        }

        void OnEnable()
        {
            GameManager.Instance.ShifterController = this;
            _spriteBoss = GetComponent<SpriteBoss>();
        }
    }
}
