﻿using UnityEngine;
using System.Collections;
using Ludum35.Engines;
using Ludum35.Managers;
using Sdn.SpriteBoss;

namespace Ludum35.Controllers
{
    public class CursorController : MonoBehaviour
    {
        private SpriteBoss _spriteBoss;

        public void Hide()
        {
            _spriteBoss.gameObject.SetActive(false);
        }

        public void Show()
        {
            _spriteBoss.gameObject.SetActive(true);
        }

        public IEnumerator MoveTo(GameObject gameObject)
        {
            yield return StartCoroutine(_spriteBoss.WaitMoveTo(gameObject.transform.position));
        }

        void OnEnable()
        {
            GameManager.Instance.CursorController = this;
            _spriteBoss = GetComponent<SpriteBoss>();
        }
    }
}