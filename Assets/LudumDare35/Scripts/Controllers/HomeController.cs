﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Ludum35.Managers;

namespace Ludum35.Controllers
{
    public class HomeController : MonoBehaviour
    {
        public Text TotalShapes;
        public Text NextUnlock;

        void Start()
        {
            GameManager.Instance.MusicController.PlayMusic("All", 0.05f);
            TotalShapes.text = GameManager.Instance.GameData.Shapes.ToString();
            NextUnlock.text = ((GameManager.Instance.GameData.UnlockedThemes) * 50).ToString();     
        }

        public void StartGame()
        {
            GameManager.Instance.SoundController.PlaySound("Button");
            SceneManager.LoadScene("Game");
        }

        public void Help()
        {
            GameManager.Instance.SoundController.PlaySound("Button");
            SceneManager.LoadScene("Help");
        }

        public void About()
        {
            GameManager.Instance.SoundController.PlaySound("Button");
            SceneManager.LoadScene("About");
        }
    }
}