﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using Ludum35.Data;
using Ludum35.Managers;

public class AboutController : MonoBehaviour {

    public ThemeDatabase ThemeDatabase;

    public SpriteRenderer[] ThemeSprites;

    public void GoHome()
    {
        GameManager.Instance.SoundController.PlaySound("Button");
        SceneManager.LoadScene("Home");
    }

    void Start()

    {
        for(int i = 0; i < ThemeSprites.Length; i++)
        {
            if (i < GameManager.Instance.GameData.UnlockedThemes)
            {
                ThemeSprites[i].sprite = ThemeDatabase.Get(i).ThemeImage;
            }
        }
    }
}
