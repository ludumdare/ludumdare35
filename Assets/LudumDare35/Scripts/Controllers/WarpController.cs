﻿using UnityEngine;
using System.Collections;
using Sdn.SpriteBoss;
using DG.Tweening;
using Ludum35.Managers;

namespace Ludum35.Controllers
{
    public class WarpController : MonoBehaviour
    {
        public int ColorIndex { get; set; }
        public int ShapeIndex { get; set; }

        private SpriteBoss _spriteBoss;

        public void Launch()
        {
            D.Trace("[WarpController] Launch");
            _spriteBoss.MoveAlongVector(Vector2.up, 3.0f);
        }

        public void Bounce()
        {
            D.Trace("[WarpController] Bounce");
            GameManager.Instance.SoundController.PlaySound("Bounce");
            StartCoroutine(bounce());
            Destroy(gameObject, 5.0f);
        }

        private  IEnumerator bounce()
        {
            D.Trace("[WarpController] bounce");
            yield return transform.FindChild("Sprite").DORotate(new Vector3(0, 0, 0), 0f).WaitForCompletion();
            transform.FindChild("Sprite").DORotate(new Vector3(0, 0, 360), 1.0f, RotateMode.FastBeyond360).SetEase(Ease.Linear).SetLoops(-1, LoopType.Restart);
            _spriteBoss.MoveInDirection(Random.Range(0, 360), 1.0f);
            yield return null;
        }

        void OnTriggerEnter2D(Collider2D collider)
        {
            D.Trace("[WarpController] OnTriggerEnter2D");
        }

        void OnEnable()
        {
            D.Trace("[WarpController] OnEnable");
            _spriteBoss = GetComponent<SpriteBoss>();
        }

    }
}