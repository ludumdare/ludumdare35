﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Ludum35.Managers;
using Ludum35.Engines;
using Ludum35.Data;

namespace Ludum35.Controllers
{
    public class GameController : MonoBehaviour
    {

        public int StartingLevel;
        public WarpController WarpPrefab;

        private int _laneIndex;
        private GameEngine _gameEngine;
        private CursorController _cursorController;
        private ShifterController _shifter;

        private LaneController _currentLane;

        public LevelData LevelData { get; set; }

        public void Init()
        {
            _laneIndex = 0;
            _currentLane = _gameEngine.LaneControllers[_laneIndex];
            StartCoroutine(moveCursor());
        }

        public void Launch()
        {
            WarpController wc = Instantiate(WarpPrefab);
            wc.transform.position = _currentLane.LaneEnd.transform.position;
            wc.ColorIndex = _currentLane.CurrentColor;
            wc.ShapeIndex = _currentLane.CurrentShape;
            wc.transform.localScale = Vector3.one * 0.5f;
            wc.transform.FindChild("Sprite").GetComponent<SpriteRenderer>().color = _currentLane.Swatch.SwatchColor[_currentLane.CurrentColor];
            wc.transform.FindChild("Sprite").GetComponent<SpriteRenderer>().sprite = _currentLane.LaneEnd.GetComponent<SpriteRenderer>().sprite;
            wc.Launch();
            GameManager.Instance.SoundController.PlaySound("Launch");
        }

        public IEnumerator NextLane()
        {
            _laneIndex += 1;

            if (_laneIndex > _gameEngine.LaneControllers.Count-1)
                _laneIndex = _gameEngine.LaneControllers.Count-1;

            _currentLane = _gameEngine.LaneControllers[_laneIndex];
            yield return StartCoroutine(moveCursor());
        }

        public IEnumerator PrevLane()
        {
            _laneIndex -= 1;

            if (_laneIndex < 0)
                _laneIndex = 0;

            _currentLane = _gameEngine.LaneControllers[_laneIndex];
            yield return StartCoroutine(moveCursor());
        }

        public IEnumerator NextColor()
        {
            D.Trace("[GameController] NextColor");
            StopAllCoroutines();
            //_cursorController.Hide();
            if (_shifter.CurrentLane == null || _shifter.CurrentLane.Id != _currentLane.Id)
            {
                GameManager.Instance.SoundController.PlaySound("RobotPaint");
                yield return StartCoroutine(_shifter.WaitMoveto(_currentLane.SpriteRenderer.gameObject.transform.position + (Vector3.down * 15.0f) + (Vector3.right * 10)));
            }
            _shifter.CurrentLane = _currentLane;
            StartCoroutine(_shifter.WaitPaint());
            _currentLane.NextColor(LevelData.Colors);
            //_cursorController.Show();
            StartCoroutine(repositionShifter());
            yield return null;
        }

        public IEnumerator NextShape()
        {
            D.Trace("[GameController] NextShape");
            StopAllCoroutines();
            //_cursorController.Hide();
            if (_shifter.CurrentLane == null || _shifter.CurrentLane.Id != _currentLane.Id)
            {
                GameManager.Instance.SoundController.PlaySound("RobotShape");
                yield return StartCoroutine(_shifter.WaitMoveto(_currentLane.SpriteRenderer.gameObject.transform.position + (Vector3.down * 15.0f) + (Vector3.right * 10)));
            }
            _shifter.CurrentLane = _currentLane;
            StartCoroutine(_shifter.WaitShift());
            _currentLane.NextShape(LevelData.Shapes);
            //_cursorController.Show();
            StartCoroutine(repositionShifter());
            yield return null;
        }

        private IEnumerator moveCursor()
        {
            StartCoroutine(_cursorController.MoveTo(_currentLane.SpriteRenderer.gameObject));
            yield return null;
        }

        private IEnumerator repositionShifter()
        {
            yield return new WaitForSeconds(1.5f);

            if (_currentLane != null)
            {
                StartCoroutine(_shifter.WaitMoveto(_currentLane.SpriteRenderer.gameObject.transform.position + (Vector3.up * 105.0f)));
            }

            _shifter.CurrentLane = null;
        }

        public void CheckBonus()
        {
            StartCoroutine(checkBonus());
        }

        private IEnumerator checkBonus()
        {
            yield return null;
        }

        void Start()
        {
            _gameEngine = GameManager.Instance.GameEngine;
            _cursorController = GameManager.Instance.CursorController;
            _shifter = GameManager.Instance.ShifterController;
            _gameEngine.StartGame();
        }

        void OnEnable()
        {
            GameManager.Instance.GameController = this;
        }
    }
}