﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Ludum35.Managers;
using DG.Tweening;

namespace Ludum35.Controllers
{
    public class GuiController : MonoBehaviour
    {
        public Text GuiText;
        public Text TextPrefab;
        public Button GameOver;
        public Button EndLevel;
        public Button NextLevel;
        public Button UnlockTheme;

        public void UpdateGui()
        {
            if (GameManager.Instance.Score > GameManager.Instance.GameData.HiScore)
                GameManager.Instance.GameData.HiScore = GameManager.Instance.Score;

            if (GameManager.Instance.GameData.Shapes > ((GameManager.Instance.GameData.UnlockedThemes) * 50))
            {
                ShowUnlock();
                GameManager.Instance.GameData.UnlockedThemes += 1;
                GameManager.Instance.SaveGameData();
            }

            string s = "";
            s += "\r\nscore: " + GameManager.Instance.Score;
            s += "\r\nhi score: " + GameManager.Instance.GameData.HiScore;
            s += "\r\nlives: " + GameManager.Instance.Lives;
            s += "\r\nshapes: " + GameManager.Instance.Shapes;
            s += "\r\nlevel: " + GameManager.Instance.Level;

            GuiText.text = s;

        }

        void OnEnable()
        {
            GameManager.Instance.GuiController = this;
            HideAll();
        }

        public void HideAll()
        {
            GameOver.gameObject.SetActive(false);
            EndLevel.gameObject.SetActive(false);
            NextLevel.gameObject.SetActive(false);
            UnlockTheme.gameObject.SetActive(false);
        }

        public void ShowGameOver()
        {
            HideAll();
            GameOver.gameObject.SetActive(true);
        }

        public void ShowEndLevel()
        {
            HideAll();
            EndLevel.gameObject.SetActive(true);
        }

        public void ShowNextLevel()
        {
            HideAll();
            NextLevel.gameObject.SetActive(true);
            NextLevel.transform.FindChild("Text").GetComponent<Text>().text = "level " + GameManager.Instance.Level.ToString();
        }

        public void ShowUnlock()
        {
            StartCoroutine(showUnlock());
        }

        private IEnumerator showUnlock()
        {

            HideAll();
            yield return UnlockTheme.transform.DOMoveY(UnlockTheme.transform.position.y - 300, 0.1f).WaitForCompletion();
            UnlockTheme.gameObject.SetActive(true);
            yield return UnlockTheme.transform.DOMoveY(UnlockTheme.transform.position.y + 300, 1.5f).WaitForCompletion();
            yield return UnlockTheme.transform.DOMoveY(UnlockTheme.transform.position.y - 300, 1.5f).WaitForCompletion();
            UnlockTheme.gameObject.SetActive(false);
        }
    }
}