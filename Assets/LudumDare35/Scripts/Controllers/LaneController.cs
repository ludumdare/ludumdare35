﻿using UnityEngine;
using System.Collections;
using Ludum35.Data;
using DG.Tweening;

namespace Ludum35.Controllers
{
    public class LaneController : MonoBehaviour
    {
        public int Width;
        public int Height;
        public ThemeData Theme;
        public SwatchData Swatch;
        public SpriteRenderer SpriteRenderer;
        public int MaxShapes;
        public int ShapeSpeed;
        public GameObject LaneStart;
        public GameObject LaneEnd;
        public System.Guid Id;

        private int _colorIndex;
        private int _shapeIndex;

        public int CurrentColor
        {
            get { return _colorIndex; }
        }

        public int CurrentShape
        {
            get { return _shapeIndex; }
        }

        public void DestroyLane()
        {
            StartCoroutine(destroyLane());
        }

        private IEnumerator destroyLane()
        {
            yield return transform.DOMoveY(transform.position.y - 150, 1.0f).WaitForCompletion();
            Destroy(gameObject);
            yield return null;
        }

        public void SetShape(int shapes, int colors)
        {
            if (shapes == -1)
                shapes = Theme.ThemeSprites.Count;

            if (colors == -1)
                colors = Swatch.SwatchColor.Count;

            _shapeIndex = Random.Range(0, shapes);
            _colorIndex = Random.Range(0, colors);
            SpriteRenderer.sprite = Theme.ThemeSprites.Get(_shapeIndex).Sprites[0];
            SpriteRenderer.color = Swatch.SwatchColor[_colorIndex];
        }

        public void NextShape(int shapes)
        {
            if (shapes == -1)
                shapes = Theme.ThemeSprites.Count;

            _shapeIndex += 1;

            if (_shapeIndex > shapes - 1)
                _shapeIndex = 0;

            SpriteRenderer.sprite = Theme.ThemeSprites.Get(_shapeIndex).Sprites[0];
        }

        public void NextColor(int colors)
        {
            if (colors == -1)
                colors = Swatch.SwatchColor.Count;

            _colorIndex += 1;

            if (_colorIndex > colors - 1)
                _colorIndex = 0;

            SpriteRenderer.color = Swatch.SwatchColor[_colorIndex];
        }

        void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            Vector3 topL = Vector3.zero;
            Vector3 topR = Vector3.right * Width;
            Vector3 BotL = Vector3.up * Height;
            Vector3 BotR = (Vector3.up * Height) + topR;
            Gizmos.DrawLine(transform.position + topL, transform.position + topR);
            Gizmos.DrawLine(transform.position + BotL, transform.position + BotR);
            Gizmos.DrawLine(transform.position + topL, transform.position + BotL);
            Gizmos.DrawLine(transform.position + topR, transform.position + BotR);
        }

        void OnEnable()
        {
            Id = System.Guid.NewGuid();
        }
    }
}
