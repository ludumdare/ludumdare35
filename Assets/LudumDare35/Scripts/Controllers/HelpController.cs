﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using Ludum35.Managers;

public class HelpController : MonoBehaviour {

    public void GoHome()
    {
        GameManager.Instance.SoundController.PlaySound("Button");        
        SceneManager.LoadScene("Home");
    }
}
