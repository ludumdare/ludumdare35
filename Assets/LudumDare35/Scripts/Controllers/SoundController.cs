﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Ludum35.Managers;

namespace Ludum35.Controllers
{
    public class SoundController : MonoBehaviour
    {
        public AudioClip[] Clips;

        private Dictionary<string, AudioClip> _clips;

        private List<GameObject> soundLoops;

        public void StopSound()
        {
            foreach (GameObject go in soundLoops)
            {
                Destroy(go.gameObject);
            }
        }

        public void PlaySound(string name, bool loop = false)
        {
            if (loop == false)
            {
                playPitchSound(_clips[name]);
            }
            else
            {
                playSoundLoop(_clips[name]);
            }
        }

        //	PRIVATE

        private void playPitchSound(AudioClip clip, float range = 0.0f)
        {
            Debug.LogWarning("6");
            playSound(clip, 1.0f + Random.Range(-range, range), 1.0f);
        }

        private void playSound(AudioClip clip)
        {

            Debug.Log("[GameSounds] playSound");
            playSound(clip, 1.0f, 0.15f);
        }

        private void playSound(AudioClip clip, float pitch, float volume)
        {

            Debug.Log("[GameSounds] playSound");

            GameObject go = new GameObject("Temp(Sound)");
            DontDestroyOnLoad(go);
            AudioSource gs = go.AddComponent<AudioSource>();
            gs.clip = clip;
            gs.volume = volume;
            gs.pitch = pitch;
            gs.Play();
            Destroy(go, clip.length + 0.1f);
        }

        private GameObject playSoundLoop(AudioClip clip)
        {

            Debug.Log("[GameSounds] playSound");
            //audio.pitch = pitch;
            //audio.PlayOneShot(clip, volume);
            //	reset
            //audio.pitch = 1.0f;

            GameObject go = new GameObject("Sound (Temp)");
            DontDestroyOnLoad(go);
            AudioSource gs = go.AddComponent<AudioSource>();
            gs.clip = clip;
            gs.loop = true;
            gs.Play();
            soundLoops.Add(go);
            return go;
        }

        void Start()
        {
        }

        void OnEnable()
        {
            D.Trace("[SoundController] OnEnable");
            GameManager.Instance.SoundController = this;
            soundLoops = new List<GameObject>();
            _clips = new Dictionary<string, AudioClip>();
            foreach (AudioClip clip in Clips)
            {
                _clips.Add(clip.name, clip);
            }
        }

        void OnDisable()
        {
        }
    }
}