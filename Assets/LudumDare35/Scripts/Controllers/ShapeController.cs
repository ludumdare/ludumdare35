﻿using UnityEngine;
using System.Collections;
using Sdn.SpriteBoss;
using Ludum35.Managers;
using DG.Tweening;

namespace Ludum35.Controllers
{
    public class ShapeController : MonoBehaviour
    {
        private SpriteBoss _spriteBoss;

        public System.Guid LaneId { get; set; }
        public int ColorIndex { get; set; }
        public int ShapeIndex { get; set; }
                
        void OnTriggerEnter2D(Collider2D collider)
        {
            D.Log("End of the Road");

            int colorIndex = -1;
            int shapeIndex = -1;

            if (collider.tag == "Warp")
            {
                WarpController wc = collider.GetComponent<WarpController>();
                colorIndex = wc.ColorIndex;
                shapeIndex = wc.ShapeIndex;

                if (ColorIndex == colorIndex && ShapeIndex == shapeIndex)
                {
                    GameManager.Instance.SoundController.PlaySound("Good");
                    StartCoroutine(good());
                    Destroy(wc.gameObject);
                }
                else
                {
                    wc.Bounce();
                    return;
                }
            }

            if (collider.tag == "Lane")
            {
                LaneController lc = collider.GetComponent<LaneController>();
                colorIndex = lc.CurrentColor;
                shapeIndex = lc.CurrentShape;

                if (ColorIndex == colorIndex && ShapeIndex == shapeIndex)
                {
                    GameManager.Instance.SoundController.PlaySound("Good");
                    StartCoroutine(good());
                }
                else
                {
                    GameManager.Instance.SoundController.PlaySound("Bad");
                    StartCoroutine(bad());
                }
                return;
            }


            Destroy(gameObject);
        }

        private IEnumerator good()
        {
            GameManager.Instance.Score += 100;
            GameManager.Instance.Shapes += 1;
            GameManager.Instance.GameData.Shapes += 1;
            GameManager.Instance.GuiController.UpdateGui();

            yield return null;
        }

        private IEnumerator bad()
        {
            GameManager.Instance.Lives -= 1;
            GameManager.Instance.GuiController.UpdateGui();

            if (GameManager.Instance.Lives <= 0)
            {
                GameManager.Instance.GameEngine.EndGame();
            }

            yield return Camera.main.transform.DOShakePosition(0.5f, 5.0f).WaitForCompletion();

            yield return null;
        }

        void OnEnable()
        {
            _spriteBoss = GetComponent<SpriteBoss>();
        }


    }
}
