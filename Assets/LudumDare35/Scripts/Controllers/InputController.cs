﻿using UnityEngine;
using System.Collections;
using Sdn.SpriteBoss;
using System;
using Ludum35.Managers;

namespace Ludum35.Controllers
{
    public class InputController : MonoBehaviour, IPauseable
    {
        private bool _paused;
        private bool _active;
        private bool _canLaunch = true;

        public bool Paused
        {
            get { return _paused; }
        }

        public void Pause()
        {
            _paused = true;
        }

        public void Resume()
        {
            _paused = false;
        }

        public void StartController()
        {
            D.Trace("[InputController] StartController");
            _active = true;
        }

        public void StopController()
        {
            D.Trace("[InputController] StopController");
            _active = false;
        }


        private IEnumerator runner()
        {
            D.Trace("[InputController] runner");


            while (true)
            {
                if (_active)
                {
                    if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
                    {
                        D.Trace("[InputController] runner Left");
                        yield return StartCoroutine(GameManager.Instance.GameController.PrevLane());
                    }

                    if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
                    {
                        D.Trace("[InputController] runner Right");
                        yield return StartCoroutine(GameManager.Instance.GameController.NextLane());
                    }

                    if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
                    {
                        D.Trace("[InputController] runner Up");
                        yield return StartCoroutine(GameManager.Instance.GameController.NextColor());
                    }

                    if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
                    {
                        D.Trace("[InputController] runner Down");
                        yield return StartCoroutine(GameManager.Instance.GameController.NextShape());
                    }

                    if (_canLaunch)
                    {
                        if (Input.GetKeyDown(KeyCode.Space))
                        {
                            D.Trace("[InputController] runner Space");
                            GameManager.Instance.GameController.Launch();
                            StartCoroutine(resetLaunch());
                        }
                    }

                    if (Input.GetKeyDown(KeyCode.LeftBracket))
                    {
                        GameManager.Instance.Level -= 1;
                        GameManager.Instance.GameEngine.EndLevel();
                    }

                    if (Input.GetKeyDown(KeyCode.RightBracket))
                    {
                        GameManager.Instance.GameEngine.EndLevel();
                    }
                }
                yield return null;
            }
        }

        private IEnumerator resetLaunch()
        {
            _canLaunch = false;
            yield return new WaitForSeconds(0.5f);
            _canLaunch = true;
        }

        void Start()
        {
            StartCoroutine(runner());
        }

        void OnEnable()
        {
            D.Trace("[InputController] OnEnable");
            GameManager.Instance.InputController = this;
        }

        void OnDisable()
        {
        }

    }
}
